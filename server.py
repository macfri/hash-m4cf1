import settings

from flask import Flask
from controller import controller

app = Flask(__name__)

app.debug = settings.DEBUG
app.secret_key = settings.SECRET_KEY

app.register_blueprint(controller)

if __name__ == '__main__':
    app.run()
