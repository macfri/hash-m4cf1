import sys
import time

from generate_token import Token

wait_for_seconds = 15

token = Token(wait_for_seconds)

seed = 'm4cf1'
#seed = token.generate_seed(uuid)
print 'seed: %s' % seed


def server():

    while True:
        print token.generate_code(seed)
        time.sleep(wait_for_seconds)

if __name__ == '__main__':

    sys_args = sys.argv

    if len(sys_args) == 2:
        if sys_args[1] == 'server':
            server()
    else:
        code = raw_input('Enter code:')
        print token.verify_code(seed, code)
