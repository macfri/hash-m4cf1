import time
import hashlib
import uuid
import settings


class Token(object):

    def __init__(self, seconds):

        self.seconds = seconds

    def _get_cuurent_time(self):

        return str(int(time.time() / self.seconds))

    def generate_code(self, seed):

        time = self._get_cuurent_time()
        ziff = hashlib. sha224(seed + time).hexdigest()
        return ''.join([x for x in ziff if x.isdigit()][:settings.CODE_LEN])

    def generate_seed(self, key):

        return '%s%s%s' % (
            str(uuid.uuid4()).replace('-', ''),
            key,
            settings.SECRET_KEY
        )

    def verify_code(self, seed, code):

        return code == self.generate_code(seed)
