import settings
import dbm


class Database(object):

    db = None

    def __init__(self):
        self.db = dbm.open(settings.DATABASE_FILE, 'c')

    def get(self, k):
        return self.db.get(k)

    def set(self, k, v):
        self.db[k] = v

    def close(self):
        self.db.close()
