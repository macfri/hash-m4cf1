import settings

from generate_token import Token
from functools import wraps
from database import Database

from flask import (session, redirect, url_for,
    render_template, request, jsonify, Blueprint)

controller = Blueprint('controller', __name__)

token = Token(settings.WAIT_FOR_TOKEN)


def autehnticated(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not session.get('email'):
            return redirect(url_for('.login'))
        return f(*args, **kwargs)
    return decorated


@controller.route('/')
def index():

    email = session.get('email')

    if email:
        return redirect(url_for('.get_token'))

    return render_template(
        'index.html'
    )


@controller.route('/login', methods=['POST', 'GET'])
def login():

    if request.method == 'POST':

        db = Database()
        email = request.form.get('email')
        exists_email = db.get(email)
        db.close()

        if exists_email:
            session['email'] = email

        return redirect(
            url_for('.register' if not exists_email else '.get_token')
        )

    else:
        return render_template(
            'login.html',
        )


@controller.route('/logout')
def logout():

    session.pop('email', None)
    return redirect(url_for('.login'))


@controller.route('/get_token')
@autehnticated
def get_token():

    db = Database()
    email = session.get('email')
    code = token.generate_code(db.get(email))
    db.close()

    return render_template(
        'token.html',
        code=code,
        wait_for_token=settings.WAIT_FOR_TOKEN * 1000,
        email=email
    )


@controller.route('/get_token_each_time', methods=['GET'])
@autehnticated
def get_token_each_time():

    db = Database()
    email = session.get('email')
    code = token.generate_code(db.get(email))
    db.close()

    return jsonify(code=code)


@controller.route('/check', methods=['POST', 'GET'])
def check():

    email = session.get('email')
    status_code = None

    if request.method == 'POST':

        email = request.form.get('email')
        code = request.form.get('code')

        db = Database()

        if not db.get(email) or not code:
            status_code = 3
        else:
            status_code = 1 if token.verify_code(
                db.get(email), code) else 2

        db.close()

    return render_template(
        'check.html',
        status_code=status_code,
        email=email
    )


@controller.route('/register', methods=['POST', 'GET'])
def register():

    status_code = None

    if request.method == 'POST':

        email = request.form.get('email')
        name = request.form.get('name')

        if not email or not name:
            status_code = 1

        else:

            db = Database()

            if db.get(email):
                status_code = 2

            else:
                db.set(email, token.generate_seed(email))
                db.set('%s_name' % email, name)
                session['email'] = email
                db.close()

                return redirect(url_for('.get_token'))

            db.close()

    return render_template(
        'register.html',
        status_code=status_code
    )
